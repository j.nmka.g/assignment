
import sys

def calculator(s: str) -> int:
    
    stack = []
    index = 0
    # handling parenteses recursively
    for c in s:
        if(c == '('):
            stack.append(index)
        if(c == ')'):
            start = stack.pop()
            # if stack is empty we consider it as end of parenteses so we replacing parenteses with calculated value
            # (multiply 2 (add (multiply 2 3) 8)) => (multiply 2 (add 6 8)) => (multiply 2 14) => 28
            if(len(stack) == 0):
                s = s[:start] + str(calculator(s[start+1:index]) )+ s[index+1:]
        index+=1
        
    if(len(stack) > 0): raise Exception("Parenteses does not match")
    
    list = s.split()
    # if we have only one item in our list 
    # we should return it because there is nothing to calculate
    if(len(list) <= 1): 
        try:
            return int(s)
        except:
            raise Exception("Invalid input")
    result = 0
    
    # first element of list should be operator name
    # rest of them are numbers 
    # multiply 2 14 7
    if(list[0] == 'add'):
        for i in range(1, len(list)):
            result+=int(list[i])
    elif(list[0] == 'multiply'):
        result = 1
        for i in range(1, len(list)):
            result*=int(list[i])
    elif(list[0] == 'exponent'): 
        # only accept 2 input for now
        if(len(list) != 3):
            raise Exception("Invalid exponent input")
        result = 1
        for i in range(0,int(list[2])):
            result *= int(list[1])
    else:
        raise Exception("Invalid operator!")
        
        
    return result
    

s = str(sys.argv[1])
print(calculator(s))

assert calculator('123') == 123, 'INTEGER failure'
assert calculator('0') == 0, 'INTEGER failure'
assert calculator('(add 1 (multiply 2 3))') ==  7, 'test 3 failed'
assert calculator('(multiply 2 (add (multiply 2 3) 8))') == 28, 'test 4 failed'
assert calculator('(multiply 1 1)') == 1, 'test 5 failed'
assert calculator('(multiply 0 (multiply 3 4))') == 0, 'test 6 failed'
assert calculator('(multiply 2 (multiply 3 4))') == 24, 'test 7 failed'
assert calculator('(multiply 3 (multiply (multiply 3 3) 3))') == 81, 'test 8 failed'
assert calculator('(add 1 1)') == 2, 'test 9 failed'
assert calculator('(add 0 (add 3 4))') == 7, 'test 10 failed'
assert calculator('(add 3 (add (add 3 3) 3))') == 12, 'test 11 failed'
assert calculator('(add 1 2 3 4 (multiply 2 3 5))') == 40, 'test 12 failed'
assert calculator('(exponent 2 3)') == 8, 'test 13 failed'
assert calculator('(add -2 3)') == 1, 'test 14 failed'


